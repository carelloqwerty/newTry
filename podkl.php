<?php
$DB = new PDO (
        "mysql:host=localhost;dbname=protest14;charset=utf8",
        'root',
        '',
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE=> PDO::FETCH_OBJ,
            PDO::ATTR_EMULATE_PREPARES => FALSE,
        ]
    );
