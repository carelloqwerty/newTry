<!DOCTYPE html>
<html lang='ru'>
<head>
    <title>Регистрация</title>
    <meta charset='utf-8'>
    <link rel="stylesheet" href="chosen/chosen.css">
<script src="jquery-3.3.1.js"></script>
    <script type="text/javascript">
        $(function(){
          $("#obl").on("change", function(){
            // AJAX-запрос
            $.ajax({
              url: "region.php?obl=" + $('#obl').val()
            })
            .done(function(data){
              $('#region').html(data);
            });
          });
        });
        $(function(){
          $("#region").on("change", function(){
            // AJAX-запрос
            $.ajax({
              url: "gorod.php?region=" + $('#reg').val()
            })
            .done(function(data){
              $('#gor').html(data);
            });
          });
        });
        
    </script>
</head>
    <body>
        <form action="registr.php">
            <p>
                <label>ФИО
                    <input name="name" type="text" required/>
                </label>
            </p>
            <p>
                <label>e-mail
                    <input name="email" type="email" required/>
                </label>
            </p>
        <?php
        // Устанавливаем соединение с базой данных
        require_once("podkl.php");
        // Формируем выпадающий список корневых разделов
        ?>
        <select id='obl' data-placeholder="Выберите область" class="chzn-select">";
            <option value='0'></option>";
            <?php $obl = $DB->query('SELECT ter_name, reg_id FROM protest14.t_koatuu_tree where ter_level=1');
            foreach ($obl->fetchAll() as $value) {?>
            <option value="<?=$value->reg_id?>"><?=$value->ter_name?></option><?php }?>
        </select>
            <p id="region"></p>
            <p id="gor"></p>
            <p>
                <input type="submit" value="Подтвердить">
            </p>    
        </form>
    </body>
    <script src="chosen/chosen.jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript"> $(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true}); </script>
</html>